import React from 'react';
import { StyleSheet } from 'react-native';
import {
  Container,
  Header,
  Title,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

const styles = StyleSheet.create({
  appLauncher: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const App = () => (
  <Container>
    <Header>
      <Left>
        <Button transparent>
          <Icon name="menu" />
        </Button>
      </Left>
      <Body>
        <Title>Header</Title>
      </Body>
      <Right />
    </Header>
    <Grid>
      <Row>
        <Col style={styles.appLauncher}>
          <Text>App 1</Text>
        </Col>
        <Col style={styles.appLauncher}>
          <Text>App 2</Text>
        </Col>
      </Row>
      <Row>
        <Col style={styles.appLauncher}>
          <Text>App 3</Text>
        </Col>
        <Col style={styles.appLauncher}>
          <Text>App 4</Text>
        </Col>
      </Row>
      <Row>
        <Col style={styles.appLauncher}>
          <Text>App 5</Text>
        </Col>
        <Col style={styles.appLauncher}>
          <Text>App 6</Text>
        </Col>
      </Row>
    </Grid>
    <Footer>
      <FooterTab>
        <Button vertical>
          <Icon name="apps" />
          <Text>Apps</Text>
        </Button>
        <Button vertical>
          <Icon name="camera" />
          <Text>Camera</Text>
        </Button>
        <Button vertical active>
          <Icon active name="navigate" />
          <Text>Navigate</Text>
        </Button>
        <Button vertical>
          <Icon name="person" />
          <Text>Contact</Text>
        </Button>
      </FooterTab>
    </Footer>
  </Container>
);

export default App;
